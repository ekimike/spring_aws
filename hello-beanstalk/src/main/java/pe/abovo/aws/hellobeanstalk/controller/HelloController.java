package pe.abovo.aws.hellobeanstalk.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringWriter;

@RestController
@RequestMapping(value = "/hello")
public class HelloController {

    @GetMapping("/{name}")
    public String helloMandarin(@PathVariable String name) {
        return "Hi " + name;
    }
}
